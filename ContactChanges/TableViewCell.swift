//
//  TableViewCell.swift
//  ContactChaneges
//
//  Created by  Masoud Moharrami on 1/31/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var contact: Contact!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(name: String, phone: String){
//        print("name: \(name), phone: \(phone)")
        nameLabel.text = name
        phoneLabel.text = phone
    }
    func configView(){
        
    }
}

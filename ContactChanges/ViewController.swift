//
//  ViewController.swift
//  ContactChaneges
//
//  Created by  Masoud Moharrami on 1/31/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//

import UIKit
import Contacts
import CoreData
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var updateTimer: Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    var user: User!
    var blockOperations = [BlockOperation]()
    
    let isLoaded = UserDefaults.standard.set(false, forKey: "isLoaded")
    
    lazy var fetchResultController: NSFetchedResultsController = { () -> NSFetchedResultsController<NSFetchRequestResult> in
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true)]
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addMockContacts()
        
        if UserDefaults.standard.bool(forKey: "isLoaded") == false {
            
            clearData()
            
            addContactsToDB(contacts: fetchCNContacts())
            UserDefaults.standard.set(true, forKey: "isLoaded")
            
            do{
                try fetchResultController.performFetch()
            }catch let err{
                print(err)
            }
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(addressBookDidChange),
            name: NSNotification.Name.CNContactStoreDidChange,
            object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    func showContacts(contacts: [TempContact], partName: String){
        print("---printing \(partName)---")
        print("----------print contacts called----------")
        for contact in contacts{
            let firstName = contact.firstName
            let lastName = contact.lastName
            let phone = contact.number
            let identifier = contact.identifier
            
            let fullName = firstName! + " " + lastName!
            print(fullName)
            print(phone!)
//            print(identifier!)
        }
        print("---end of printing \(partName)---")
    }
    @IBAction func arrowUpClicked(_ sender: Any) {
        let index = IndexPath(row: 0, section: 0)
        tableView.scrollToRow(at: index, at: .top, animated: true)
    }
    // MARK: STATE CHANGED
    @objc func stateChanged(){
        let state = getContactState()
        
        switch state{
        case 1: // deleted
            //                    findDeletedContact()
            print("-------------Deleted-------------")
            let contacts = findDeletedContacts()
            updateDBwithDeleted(contacts: contacts)
            break
        case 2: // added
            print("------------Added-------------")
            let contacts = findAddedContacts()
            showContacts(contacts: contacts, partName: "findAddedContacts in stateChanged")
            updateDBwithAdded(contacts: contacts)
            break
        case 3: // changed
            print("-------------Changed-------------")
            let contacts = findChangedContacts()
            showContacts(contacts: contacts, partName: "findChangedContacts in stateChanged")
            updateDBwithChanged(contacts: contacts)
            break
        default:
            break
            
        }
    }
    // MARK: tableView delegation and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        if let count = fetchResultController.sections?.count{
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if let count = fetchResultController.sections?[section].numberOfObjects{
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell
            
            let contact = fetchResultController.object(at: indexPath) as? Contact
            if tableView.numberOfRows(inSection: 0) != 0{
//                print("\(contact?.firstName) \(contact?.lastName)")
                
                let name = (contact?.firstName)! + " " + (contact?.lastName)!
                let phone: String = (contact?.number)!
//                print("name: \(name), phone: \(phone)")
                
                cell?.configureCell(name: name, phone: phone)
                
//                print("db contacts: \(tableView.numberOfRows(inSection: 0))")
                UserDefaults.standard.set(tableView.numberOfRows(inSection: 0), forKey: "dbContacts")
                
                return cell!
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.isHighlighted = false
        cell?.isSelected = false
        print("row: \(indexPath.row)")
    }
    // MARK: contact funcs

    func addContactsToDB(contacts: [CNContact]){
        
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.getContext()
        if user == nil{
            user = NSEntityDescription.insertNewObject(forEntityName: "User", into: context) as? User
        }
        
        for contact in contacts{
            
            //            print("Given name: \(contact.givenName), last name: \(contact.familyName)")
            
//            let contactName = contact.givenName + " " + contact.familyName
//            print(contactName)
//            print(contact.identifier)
            let numbers = contact.phoneNumbers
            
            for number in numbers{
                
//                print(number.value.stringValue)
                let value = trimNumber(number: number.value.stringValue)
                
                _ = createContact(user: user!, firstName: contact.givenName, lastName: contact.familyName, phone: value, identifier: contact.identifier, context: context)
                
                appDelegate.saveContext()
                
//                print("\(contactName), \(value)")
                
            }
        }
        
    }
    func addContactToDB(contact: CNContact){
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.getContext()
        
        let contactName = contact.givenName + " " + contact.familyName
        print(contactName)
        
        let numbers = contact.phoneNumbers
        
        for number in numbers{

            let value = trimNumber(number: number.value.stringValue)
            
            _ = createContact(user: user!, firstName: contact.givenName, lastName: contact.familyName, phone: value, identifier: contact.identifier, context: context)
            
            appDelegate.saveContext()
            
            print("\(contactName), \(value)")
            
        }
    }
    func fetch(_ completion: () -> Void) {
        print("Edit ended")
        completion()
    }
    func fetchCNContacts() -> [CNContact]{
        
        var contacts = [CNContact]()
        var filteredList = [CNContact]()
        let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactIdentifierKey]
        var allContainers: [CNContainer] = []
        let store = CNContactStore()
        
        do {
            allContainers = try store.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let containerResults = try store.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as [CNKeyDescriptor])
             
                // Put them into "contacts"
                contacts.append(contentsOf: containerResults)
                
            } catch {
                print("Error fetching results for container")
            }
        }
//        print("unfiltered count: \(contacts.count)")
        for i in 0...contacts.count-1{
            if contacts[i].phoneNumbers.count != 0{
                filteredList.append(contacts[i])
            }
        }
//        print("phone contacts: \(filteredList.count)")
        
        return filteredList
    }
    func fetchCNContact(identifier: String) -> CNContact{
        
        let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactIdentifierKey]
        let store = CNContactStore()

        do {
            let result = try store.unifiedContact(withIdentifier: identifier, keysToFetch: keysToFetch as [CNKeyDescriptor])
            
            return result
        } catch {
            print("Error fetching results for container")
        }

        return CNContact()
    }
    func createContact(user: User, firstName: String, lastName: String, phone: String,identifier: String, context: NSManagedObjectContext) -> Contact{
        
        let contact = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: context) as? Contact
        
        contact?.firstName = firstName
        contact?.lastName = lastName
        contact?.number = phone
        contact?.identifier = identifier
//        print(identifier)
        user.addToContacts(contact!)
        
        return contact!
    }
    
    func createContactOffline(user: User, firstName: String, lastName: String, phone: String,identifier: String, context: NSManagedObjectContext) -> Contact{
        
        let contact = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: context) as? Contact
        
        contact?.firstName = firstName
        contact?.lastName = lastName
        contact?.number = phone
        contact?.identifier = identifier
        
        return contact!
    }
    
    func trimNumber(number: String) -> String{
        var result: String = "+"
        if number[number.startIndex] == "+"{
            result = result + number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            return result
        }
        return number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    func clearData(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.getContext()
        do{
            let entityNames = ["User", "Contact"]
            for entityName in entityNames{
                let request =  NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
                let objects = try(context.fetch(request)) as? [NSManagedObject]
                for object in objects!{
                    context.delete(object)
                }
            }
            try (context.save())
        }catch let err{
            print(err)
        }
    }
    func getContactState() -> Int{
        let dbCount = UserDefaults.standard.integer(forKey: "dbContacts")
        var phoneNumbers = 0
        for contact in fetchCNContacts(){
            let numbers = contact.phoneNumbers
            
            for _ in 0...numbers.count - 1{
                phoneNumbers = phoneNumbers + 1
            }
        }
        
        print("contact numbers: \(phoneNumbers)")
        print("db contacts: \(dbCount)")
        if dbCount > phoneNumbers {
            print("1 contact deleted!")
            return 1 // delete state
        }else if dbCount < phoneNumbers {
            print("1 contact added!")
            return 2 // add state
        }
        if dbCount == phoneNumbers{
            print("1 contact changed")
            return 3 // change state
        }
        
        return 0
    }

    // MARK: Contact changes Delegation Funcs
    func findAddedContacts() -> [TempContact]{
        
        let changed = findChangedContacts()
        let cmd = CMD()
        var addedContacts = [TempContact]()
        
        for c in cmd{
            
            var found = false
            
            for contact in changed{
                if c.identifier == contact.identifier{
                    found = true
                }
            }
            if found == false {
                addedContacts.append(c)
            }
        }
        return addedContacts
    }
    
    func findDeletedContacts() -> [TempContact]{
        
        let databaseMinusContacts = DMC()
        let changedContacts = findChangedContacts()
        var deletedContacts = [TempContact]()
        
        
        for dmc in databaseMinusContacts{
            
            var flag = false
            
            for chn in changedContacts{
                
                if dmc.number == chn.number{
                    flag = true
                }
            }
            if flag == false{
                deletedContacts.append(dmc)
            }
        }
        if databaseMinusContacts.count != 0 {
            print("Number of deleted contacts: \(deletedContacts.count)")
        }
        return databaseMinusContacts
    }
    
    func findChangedContacts() -> [TempContact]{

        let contacts = CMD()
        var changedDBContacts = [TempContact]()
        var isNotSame = [TempContact]()
        for contact in contacts{
            
            if isContactByIdentifierFound(identifier: contact.identifier!) == true{
//                print(contact.identifier)
                
                changedDBContacts.append(contentsOf: getDBContactsByIdentifier(identifier: contact.identifier!))
            }
        }
        if changedDBContacts.count == 0{
            print("There is a problem in chnaged contacts!!!")
            changedDBContacts = []
        }else{
        // find contact that changed in phone contacts
            for i in 0...changedDBContacts.count - 1{
                let c = fetchCNContact(identifier: changedDBContacts[i].identifier) as CNContact
                if c.identifier != ""{
                    let numbers = c.phoneNumbers
                    
                    var flag = false
                    for number in numbers{
                        let phone = trimNumber(number: number.value.stringValue)
                        
                        if phone == changedDBContacts[i].number {
                            flag = true
                        }
                    }
                    if flag == false{
                        isNotSame.append(changedDBContacts[i])
                    }
                }
            }
        }
        if isNotSame.count == 0{
            isNotSame = []
        }
        return isNotSame
    }
    // Contacts - Database
    func CMD() -> [TempContact]{
        
        let contacts = fetchCNContacts()
        var contactsDif = [TempContact]()
        
        for contact in contacts{
            let numbers = contact.phoneNumbers
            
            for number in numbers{
                
                let phone = trimNumber(number: number.value.stringValue)
                
                if findContactByPhone(number: phone) == false {
                    
                    let newContact = TempContact(firstName: contact.givenName, lastName: contact.familyName, identifier: contact.identifier, number: phone)

                    contactsDif.append(newContact)
                }
            }
        }
        if contactsDif.count == 0 {
            print("CMD is nil")
            contactsDif = []
        }else{
            showContacts(contacts: contactsDif, partName: "*in CMD*")
        }
        return contactsDif
    }
    // Database - Contacts
    func DMC() -> [TempContact]{

        let contacts = fetchCNContacts()
        let dbContacts = user.contacts?.allObjects as? [Contact]
        var resultContacts = [TempContact]()
        
        for dbc in dbContacts!{
            
            var flag = false
            
            for contact in contacts{
                
                let numbers = contact.phoneNumbers
                
                for number in numbers{
                    let phone = trimNumber(number: number.value.stringValue)
                    
                    if dbc.number == phone {
                        flag = true
                    }
                }
            }
            if flag == false{
                let newContact = TempContact(firstName: dbc.firstName, lastName: dbc.lastName, identifier: dbc.identifier, number: dbc.number)
                resultContacts.append(newContact)
            }
        }
        showContacts(contacts: resultContacts, partName: "* in DMC *")
        return resultContacts
    }

    //MARK: Finding Contacts
    func findContactByPhone(number: String) -> Bool{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetchRequest.predicate = NSPredicate(format: "number = %@", number)
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        
        do {
            let contacts = try context?.fetch(fetchRequest)
            assert((contacts?.count)! < 2) // we shouldn't have any duplicates in CD
            
            if (contacts?.first as? Contact) != nil {
//                print(contact.firstName! + " " + contact.lastName!)
//                print(contact.number!)
                return true
            } else {
                print("There is no contact with this phone number")
            }
        } catch {
            print(error)
        }
        return false
    }
    func getContactByPhone(number: String) -> CNContact{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetchRequest.predicate = NSPredicate(format: "number = %@", number)
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        
        do {
            let contacts = try context?.fetch(fetchRequest)
            assert((contacts?.count)! < 2) // we shouldn't have any duplicates in CD
            
            if let contact = contacts?.first as? CNContact {
                //                print(contact.firstName! + " " + contact.lastName!)
                //                print(contact.number!)
                
                return contact
            } else {
                print("There is no contact with this phone number")
            }
        } catch {
            print(error)
        }
        return CNContact()
    }
    func isContactByIdentifierFound(identifier: String) -> Bool{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", identifier)
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        
        do {
            let contacts = try context?.fetch(fetchRequest)
//            assert((contacts?.count)! < 2) // we shouldn't have any duplicates in CD
            if contacts != nil{

                if (contacts?.first as? Contact) != nil{
                    //                print(contact.firstName! + " " + contact.lastName!)
                    //                print(contact.number!)
                    return true
                } else {
                    print("There is no contact with this identifier")
                }
            }
        } catch {
            print(error)
        }
        return false
    }
    func getDBContactsByIdentifier(identifier: String) -> [TempContact]{
        print(String(identifier))
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", identifier)
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        
        var fetchedContacts = [TempContact]()
        
        do {
            let contacts = try context?.fetch(fetchRequest)
            
            if contacts != nil{
                for i in 0...(contacts?.count)! - 1{
                    let contact = contacts![i] as? Contact
                    let temp = TempContact(firstName: contact?.firstName, lastName: contact?.lastName, identifier: contact?.identifier, number: contact?.number)
                    fetchedContacts.append(temp)
                }
                //                if let contact = contacts?.first as? Contact{
                //                    let tempContact = TempContact(firstName: contact.firstName, lastName: contact.lastName, identifier: contact.identifier, number: contact.number)
                //                    return [tempContact]
                //                } else {
                //                    print("There is no contact with this identifier")
                //                }
            }else {
                print("There is no contact with this identifier")
            }
        } catch {
            print(error)
        }
        return fetchedContacts
    }
    func getPhoneContactByIdentifier(identifier: String) -> TempContact{
        print(String(identifier))
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", identifier)
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        
        do {
            let contacts = try context?.fetch(fetchRequest)
            
            if contacts != nil{
                
                if let contact = contacts?.first as? Contact{
                    let tempContact = TempContact(firstName: contact.firstName, lastName: contact.lastName, identifier: contact.identifier, number: contact.number)
                    return tempContact
                } else {
                    print("There is no contact with this identifier")
                }
            }
        } catch {
            print(error)
        }
        return TempContact()
    }
    func getDBContactById(identifier: String){
        
        let contacts = user.contacts?.allObjects as! [Contact]
        
        for contact in contacts{
            if contact.identifier == identifier{
                print("\(String(describing: contact.firstName)): \(String(describing: contact.number))")
            }
        }
        
    }
    // MARK: Updating Functions
    func updateDBwithChanged(contacts: [TempContact]){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate?.getContext()
        var contactsToDelete = [Contact]()
        
        let cmd = CMD()
        for c in contacts{

            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
            fetchRequest.predicate = NSPredicate(format: "number = %@", c.number)
            
            do {
                let contacts = try context?.fetch(fetchRequest) as! [Contact]
                
                if contacts.count != 0{
                    for i in 0...(contacts.count) - 1{
                        let contact = contacts[i]
                        if contact.number == c.number{
                            contactsToDelete.append(contact)
                        }
                    }
                }
            } catch {
                print(error)
            }
        }
        for contact in contactsToDelete{
            context?.delete(contact)
            appDelegate?.saveContext()
        }
        for contact in cmd {
            _ = createContact(user: user, firstName: contact.firstName, lastName: contact.lastName, phone: contact.number, identifier: contact.identifier, context: context!)
            appDelegate?.saveContext()
        }
        
        
        tableView.reloadData()
    }
    func updateDBwithAdded(contacts: [TempContact]){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate?.getContext()
        
        for contact in contacts{
            _ = createContact(user: user, firstName: contact.firstName, lastName: contact.lastName, phone: contact.number, identifier: contact.identifier, context: context!)
            appDelegate?.saveContext()
            let count = UserDefaults.standard.integer(forKey: "dbContacts")
            UserDefaults.standard.set(count + 1, forKey: "dbContacts")
        }

    }
    func updateDBwithDeleted(contacts: [TempContact]){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate?.getContext()
        
        for contact in contacts{
        
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
            fetchRequest.predicate = NSPredicate(format: "number = %@", contact.number)
            
            do {
                let contacts = try context?.fetch(fetchRequest) as! [Contact]
                
                if let contact = contacts.first as? Contact{
                    
                    context?.delete(contact)
                    appDelegate?.saveContext()
                    let count = UserDefaults.standard.integer(forKey: "dbContacts")
                    UserDefaults.standard.set(count - 1, forKey: "dbContacts")
                } else {
                    print("There is no contact with this identifier")
                }
            } catch {
                print(error)
            }
        }
    }
    // MARK: Data Controlling
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        print("object: \(anObject), New IndexPath: \(String(describing: newIndexPath))")
        switch (type) {
        case .insert:
            if newIndexPath != nil {
                blockOperations.append(BlockOperation(block: {
                    self.tableView.insertRows(at: [newIndexPath!], with: UITableViewRowAnimation.fade)
                    print("blockoperations.append")
                }))
            }
            break;
        case .delete:
            if let indexPath = indexPath {
//                blockOperations.append(BlockOperation(block: {
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    print("blockoperations.delete")
//                }))
            }
            break;
        case .update:
            if indexPath != nil {
                print("in Update state")
                self.tableView.reloadData()
            }
            break;
        case .move:

            break
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.performBatchUpdates({
            for operation in self.blockOperations{
                operation.start()
            }

        }, completion: {(completed) in
            let numberString = String(describing: self.fetchResultController.sections? [0].numberOfObjects)
            let number = self.fetchResultController.sections? [0].numberOfObjects
            print("db count: \(numberString)")

            let lastItem = number! - 1
            let indexPath = IndexPath(item: lastItem, section: 0)
            print("did changed: indexPath: \(indexPath.row)")

            if indexPath.row != -1 {
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }else {
                print("row number is -1")
            }
        })
    }
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
    }
    
    // MARK: Change in contacts func
    @objc func addressBookDidChange(notification: NSNotification){
        stateChanged()
    }
    
    // MARK: functions to run service in background
    @objc func reinstateBackgroundTask() {
        if updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    func addMockContacts(){
        
        let unknownError = NSError(domain: "JSON File", code: 0, userInfo: [NSLocalizedDescriptionKey: "Problem in JSON"])
        
        if let path = Bundle.main.path(forResource: "test", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                
//                print("jsonData:\(jsonObj)")
                guard let items = jsonObj.array else {
                    throw unknownError
                }
                for item in items{
                    print(item)
                    print("-------------")
                    
                    let newContact = CNMutableContact()
                    newContact.givenName = item["firstname"].string ?? ""
                    newContact.familyName = item["surename"].string ?? ""
                    
                    let email = CNLabeledValue(label:CNLabelWork, value:(item["email"].string ?? "") as NSString)
                    newContact.emailAddresses = [email]
                    
                    newContact.phoneNumbers = [CNLabeledValue(
                        label:CNLabelPhoneNumberiPhone,
                        value:CNPhoneNumber(stringValue:item["phone"].string ?? ""))]
                    do {
                        let saveRequest = CNSaveRequest()
                        saveRequest.add(newContact, toContainerWithIdentifier: nil)
                        let store = CNContactStore()
                        try store.execute(saveRequest)
                    } catch {
                        print("Unable to save the new contact.")
                    }
                    
                }
                
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
    }
}
struct TempContact{
    var firstName: String!
    var lastName: String!
    var identifier: String!
    var number: String!
}

//
//  Contact+CoreDataProperties.swift
//  ContactChaneges
//
//  Created by  Masoud Moharrami on 2/8/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var identifier: String?
    @NSManaged public var lastName: String?
    @NSManaged public var number: String?
    @NSManaged public var user: User?

}
